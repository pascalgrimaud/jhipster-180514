import { NgModule } from '@angular/core';

import { Jhipster180514SharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
    imports: [Jhipster180514SharedLibsModule],
    declarations: [JhiAlertComponent, JhiAlertErrorComponent],
    providers: [],
    exports: [Jhipster180514SharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class Jhipster180514SharedCommonModule {}
